<?php

/**
 * Class StaticData_Version
 * api call for staticData version
 */
class StaticData_Version
{
    private $version;

    public function loadFromAPI($region,$api_key){
        $url  = "https://global.api.pvp.net/api/lol/static-data/$region/v1.2/versions?api_key=$api_key";
        $json = file_get_contents($url);
        $data = json_decode($json);

        $this->version = $data[0];
    }

    public function getVersion(){
        return $this->version;
    }
}