<?php

/**
 * Class StaticData_ProfileIcon
 * profile icon object
 */
class StaticData_ProfileIcon
{
    private $url;
    private $data_version;

    public function __construct()
    {
        $version = new StaticData_Version();
        $version->loadFromAPI("euw", Core::API_KEY);

        $this->data_version = $version->getVersion();
    }

    public function loadFromId($id){
        $this->url = "http://ddragon.leagueoflegends.com/cdn/$this->data_version/img/profileicon/$id.png";
        return $this;
    }

    public function getUrl(){
        return $this->url;
    }
}