<?php

/**
 * Class Helper_HTML_Img
 * -- HTML <img> element
 */
class Helper_HTML_Img extends Helper_HTML_Base
{
    public function __construct($src = false)
    {
        parent::__construct('img');
        if($src)
            parent::addAttr(array(
               'src' => $src
            ));
    }
}