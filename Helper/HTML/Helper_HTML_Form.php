<?php

/**
 * Class Helper_HTML_Form
 * -- HTML <form> element
 */
class Helper_HTML_Form extends Helper_HTML_Base
{
    // TODO js call
    private $query;

    private $buttonClass;
    private $buttonText = "Envoyer"; //TODO add to constants (?)
    private $hasButton  = true;

    private $attr = array();

    public function __construct()
    {
        parent::__construct('form');
    }

    public function setMethod($method){
        $this->attr['method'] = $method;
        return $this;
    }

    public function setAction($action){
        $this->attr['action'] = $action;
        return $this;
    }

    public function setQuery($query){
        $this->query = $query;
        return $this;
    }

    public function setButtonText($text){
        $this->buttonText = $text;
        return $this;
    }

    public function setButtonClass($buttonClass)
    {
        $this->buttonClass = $buttonClass;
        return $this;
    }

    // remove form button
    public function disableButton(){
        $this->hasButton = false;
        return $this;
    }

    // add attributes before returning html
    public function get()
    {
        if(!empty($this->attr))
            parent::addAttr($this->attr);

        if($this->hasButton){
            $button = new Helper_HTML_Base('input');
            $button_attr = array(
                'type'  => 'submit',
                'value' => $this->buttonText
            );

            if($this->query)
                $button_attr['onclick'] = $this->query.'()';

            if($this->buttonClass)
                $button->setClass('button '.$this->buttonClass);
            else
                $button->setClass('button');

            $button->addAttr($button_attr);

            $this->addContent($button->get());

        }
        return parent::get();
    }
}