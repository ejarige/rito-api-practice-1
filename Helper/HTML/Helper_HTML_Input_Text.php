<?php

/**
 * Class Helper_HTML_Input_Text
 * -- HTML <input type="text"> element
 */
class Helper_HTML_Input_Text extends Helper_HTML_Base
{
    public function __construct()
    {
        parent::__construct('input');
        $this->addAttr(array(
            'type' => 'text'
        ));
    }
}