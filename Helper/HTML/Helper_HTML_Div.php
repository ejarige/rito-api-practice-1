<?php

/**
 * Class Helper_HTML_Div
 * -- HTML <div> element
 */
class Helper_HTML_Div extends Helper_HTML_Base
{
    public function __construct()
    {
        parent::__construct('div');
    }
}