<?php

/**
 * Class Helper_HTML_Base
 * -- HTML base element
 */
class Helper_HTML_Base
{
    // element type (e.g.: h1, p, b, img, ...)
    private $type;

    // element attributes
    private $id;
    private $name;
    private $class;

    // element additional attributes
    private $attr;

    // element content
    private $content;

    public function __construct($type,$hasId = true)
    {
        if($hasId)
            $this->id   = uniqid();
        $this->type = $type;
    }

    public function setType($type){
        $this->type = $type;
        return $this;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /*
     * $attr : string or array
     */
    public function addAttr($attr)
    {
        if(!$this->attr || $attr != (array) $attr)
            $this->attr = $attr;
        else
           $this->attr = array_merge($this->attr,$attr);
        return $this;
    }

    /*
     * $content : string
     */
    public function addContent($content)
    {
        if(!$this->content)
            $this->content = $content;
        else
            $this->content .= $content;
        return $this;
    }

    /*
     * return html source string
     */
    public function get(){
        $html = '<'.($this->type ? $this->type.' ' : '')
            .($this->id ? 'id="'.$this->id.'"' : '')
            .($this->name ? ' name="'.$this->name.'"' : '')
            .($this->class ? ' class="'.$this->class.'" ' : '');

        if($this->attr)
            if(is_array($this->attr))
                foreach($this->attr as $k => $v)
                    $html .= $k.'="'.$v.'" ';
            else
                $html .= $this->attr.' ';

        if($this->content)
            $html .= '>'.$this->content.'</'.$this->type.'>';
        else
            $html .= '/>';

        return $html;
    }
}