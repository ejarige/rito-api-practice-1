<?php

/**
 * Class Template_Search
 * template w/ search bar & results
 */

class Template_Search extends Template_Base
{

    private $content;

    public function __construct()
    {
        parent::__construct("Find a summoner");

        // Page
        $main_div = new Helper_HTML_Div();
        $main_div->setClass("main_div");

        // Search bar
        $search_div = new Helper_HTML_Div();
        $search_div->setClass("search_div");

        $input = new Helper_HTML_Input_Text();
        $input
            ->setName('summ_name')
            ->addAttr(array(
                'placeholder' => 'Find a summoner'
            ));

        if(!empty($_GET['summ_name']))
            $input->addAttr(array(
                'value' => $_GET['summ_name']
            ));

        $form = new Helper_HTML_Form();
        $form
            ->setName('summ_form')
            ->setQuery('query_summoner')
            ->setButtonText('Search')
            ->addContent($input->get());

        $search_div->addContent($form->get());

        $main_div->addContent($search_div->get());

        // result div
        $game_div = new Helper_HTML_Div();
        $game_div->setClass('game_div');

        if(!empty($_GET['summ_name'])){
            $input->addAttr(array(
                'value' => $_GET['summ_name']
            ));

            $teamA_div = new Helper_HTML_Div();
            $teamA_div
                ->setClass('team_div')
                ->addContent("<hr/><h1>TEAM A</h1><hr/>");

            $teamB_div = new Helper_HTML_Div();
            $teamB_div
                ->setClass('team_div')
                ->addContent("<hr/><h1>TEAM B</h1><hr/>");

            // create summoner object from query
            $summoner = new RitoData_Summoner();
            $summoner->load('euw',$_GET['summ_name']);

            // retrieve game data from summoner
            $current_game = new RitoData_CurrentGame();
            $current_game->loadBySummonerName('euw',$_GET['summ_name']);

            // format players' div
            foreach($current_game->getParticipants() as $p){
                $summ_temp = new Template_Summoner($p['summoner']);

                $summ_div = new Helper_HTML_Div();
                $summ_div
                    ->setClass('summ_div')
                    ->addContent($summ_temp->getContent());

                // add players in their teams
                if($p['teamId'] == 100)
                    $teamA_div->addContent($summ_div->get());
                else
                    $teamB_div->addContent($summ_div->get());
            }

            // add teams to game
            $game_div
                ->addContent($teamA_div->get())
                ->addContent($teamB_div->get());

            // add game to page
            $main_div->addContent($game_div->get());
        }
        // add all to base template
        $this->content = $main_div->get();
    }

    // return html source
    public function get(){
        parent::setBody($this->content);
    }
}