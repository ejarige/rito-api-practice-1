<?php

/**
 * Class Template_Base
 * Base template
 */
abstract class Template_Base
{
    protected $head;
    protected $title;
    protected $body;

    public function __construct($title = false)
    {
        $this->head = '
            <meta charset="utf-8" />
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />';

        foreach(Core::CSS as $css)
            $this->head .= '<link rel="stylesheet" href="'.$css.'" />';

        foreach(Core::JS as $js)
            $this->head .= '<script type="text/javascript" src="'.$js.'" />';

        if($title)
                $this->title = $title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    // create and fill all html elements of a page
    public function show(){
        /*
         * HEAD
         */
        $head_html = new Helper_HTML_Base('head',false);

        $metas =
            '<meta charset="utf-8" />
            <meta http-equiv="x-ua-compatible" content="ie=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />';

        $head_html->addContent($metas);

        foreach (Core::CSS as $css){
            $css_html = new Helper_HTML_Base('link',false);
            $css_html->addAttr(array(
                'rel'  => 'stylesheet',
                'href' => $css
            ));
            $head_html->addContent($css_html->get());
        }

        foreach (Core::JS as $js){
            $js_html = new Helper_HTML_Base('script',false);
            $js_html->addAttr(array(
                'type'  => 'text/javascript',
                'src' => $js
            ));
            $js_html->addContent(' ');
            $head_html->addContent($js_html->get());
        }

        $title_html = new Helper_HTML_Base('title',false);
        if($this->title)
            $title_html->addContent($this->title);
        else
            $title_html->addContent(Core::PROJECT_NAME);
        $head_html->addContent($title_html->get());

        /*
         * BODY
         */
        $body_html = new Helper_HTML_Base('body',false);
        $body_html->addContent($this->body);

        /*
         * PAGE
         */
        $page_html = new Helper_HTML_Base('html',false);
        $page_html
            ->addContent($head_html->get())
            ->addContent($body_html->get());

        echo $page_html->get();
    }
}