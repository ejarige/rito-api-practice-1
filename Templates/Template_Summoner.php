<?php

/**
 * Class Template_Summoner
 * formats data for 1 summoner
 */

class Template_Summoner extends Template_Base
{
    private $summoner;
    private $content;

    public function __construct(RitoData_Summoner $summoner)
    {
        $this->summoner = $summoner;

        // create html base template
        parent::__construct($summoner->getName());

        // profile pic
        $profile_pic = new Helper_HTML_Img($summoner->getIconUrl());

        // summoner name
        $name = new Helper_HTML_Base('h1');
        $name->addContent($summoner->getName());

        // summoner rank infos
        $rank_info = new Helper_HTML_Base('h2');
        $rank_info->addContent('Summoner level '.$summoner->getLevel().' - ');

        if($summoner->getRank()!="Unranked")
            $rank_info->addContent(
                $summoner->getRank().' '
                .$summoner->getDivision().' ('
                .$summoner->getLp()
                .' LP)'
            );
        else
            $rank_info->addContent($summoner->getRank());

        // summoner additional infos
        $games_info = new Helper_HTML_Base('h3');
        $games_info
            ->addContent('W : '.$summoner->getWins().' / L : '.$summoner->getLosses().' ('.number_format($summoner->getWinRatio(),2,',','').'%)');

        // adding all to div
        $main_div = new Helper_HTML_Div();
        $main_div
            ->setClass('banner')
            ->addContent($profile_pic->get())
            ->addContent($name->get())
            ->addContent($rank_info->get())
            ->addContent($games_info->get());

        $this->content = $main_div->get();
    }

    // get content only (w/o headers)
    public function getContent()
    {
        return $this->content;
    }

    // get full html page
    public function get(){
        parent::setBody($this->content);
    }
}