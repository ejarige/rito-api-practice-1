<?php

class Core
{
    // Project name
    const PROJECT_NAME = "Rito API toast";

    // PHP Directories
    const PHP = array(
        "Helper/HTML/",
        "Query/",
        "RitoData/",
        "StaticData/",
        "Templates/"
    );

    // CSS Files
    const CSS = array(
        "Styles/default.css",
        "Plugins/foundation/css/foundation.css",
        "Plugins/foundation/css/app.css"
    );

    // JS Files
    const JS = array(
        "Plugins/JQuery/jquery-1.12.3.min",
        "Scripts/default.js"
    );

    // Rito API key
    const API_KEY  = "12345678-1234-1234-1234-123456789012";

    // Debug functions
    static function wtfIsThis($x){
        echo "<pre>";
        print_r($x);
        echo "</pre>";
    }
}