<?php
/**
 * Autoload all required files
 * Configure directories in Config/Core.php
 */
require_once("Config/Core.php");

function __autoload($class_name) {
    foreach (Core::PHP as $directory) {
        if (file_exists($directory . $class_name . '.php')) {
            require_once ($directory . $class_name . '.php');
            return;
        }
    }
}