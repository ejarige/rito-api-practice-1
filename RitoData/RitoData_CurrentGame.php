<?php

/**
 * Class RitoData_CurrentGame
 * -- Current game object
 * -- Contains all data about current game
 */
class RitoData_CurrentGame //TODO exceptions
{
    private $gameLength;
    private $gameMode;
    private $mapId;
    private $bannedChampions;
    private $gameType;
    private $gameId;
    private $gameQueueConfigId;
    private $platformId;

    private $participants = array();

    public function loadBySummonerName($region,$name){
        $api_key = Core::API_KEY;

        $summoner = new RitoData_Summoner();
        $summoner->load($region,$name);

        // Getting game data
        $url  = "https://euw.api.pvp.net/observer-mode/rest/consumer/getSpectatorGameInfo/EUW1/"
            .$summoner->getId()."?api_key=$api_key";
        $json = file_get_contents($url);
        $gameData = json_decode($json,1);

        $this->gameLength        = $gameData["gameLength"];
        $this->gameMode          = $gameData["gameMode"];
        $this->mapId             = $gameData["mapId"];
        $this->bannedChampions   = $gameData["bannedChampions"];
        $this->gameType          = $gameData["gameType"];
        $this->gameId            = $gameData["gameId"];
        $this->gameQueueConfigId = $gameData["gameQueueConfigId"];
        $this->gameType          = $gameData["gameType"];
        $this->platformId        = $gameData["platformId"];

        // Retrieving all participants
        foreach ($gameData["participants"] as $p)
            $this->participants[$p['summonerId']] = $p;

        // Creating all participants ID string
        $allParticipants = '';
        foreach($this->participants as $p)
            $allParticipants .= $p["summonerId"].',';
        $allParticipants = rtrim($allParticipants,',');

        // Summoner Information
        $url  = "https://euw.api.pvp.net/api/lol/$region/v1.4/summoner/$allParticipants?api_key=$api_key";
        $json = file_get_contents($url);
        $summData = json_decode($json,1);

        foreach($summData as $key => $summoner){
            $tempSumm = new RitoData_Summoner();

            $tempSumm
                ->setId($summData[$key]["id"])
                ->setName($summData[$key]["name"])
                ->setLevel($summData[$key]["summonerLevel"])
                ->setProfileIconId($summData[$key]["profileIconId"])
                ->setRevisionDate($summData[$key]["revisionDate"]);

            $this->participants[$tempSumm->getId()]["summoner"] = $tempSumm;
        }

        // Rank Information
        $url = "https://euw.api.pvp.net/api/lol/$region/v2.5/league/by-summoner/$allParticipants/entry?api_key=$api_key";
        $json = file_get_contents($url);
        $leagueData = json_decode($json,1);

        foreach ($this->participants as $participant) {
            if(isset($leagueData[strval($participant["summoner"]->getId())])){
                foreach($leagueData[strval($participant["summoner"]->getId())] as $rankData)
                    if($rankData["queue"] == "RANKED_SOLO_5x5"){
                        $participant["summoner"]
                            ->setRank($rankData["tier"])
                            ->setDivision($rankData["entries"][0]["division"])
                            ->setLp($rankData["entries"][0]["leaguePoints"])
                            ->setWin($rankData["entries"][0]["wins"])
                            ->setLose($rankData["entries"][0]["losses"]);
                    }
            }
            // No rank information
            else{
                $summoner
                    ->setRank("Unranked")
                    ->setWin(0)
                    ->setLose(0);
            }
        }
    }

    public function getGameLength()
    {
        return $this->gameLength;
    }

    public function getGameMode()
    {
        return $this->gameMode;
    }

    public function getMapId()
    {
        return $this->mapId;
    }

    public function getBannedChampions()
    {
        return $this->bannedChampions;
    }

    public function getGameType()
    {
        return $this->gameType;
    }

    public function getGameId()
    {
        return $this->gameId;
    }

    public function getGameQueueConfigId()
    {
        return $this->gameQueueConfigId;
    }

    public function getPlatformId()
    {
        return $this->platformId;
    }

    public function getParticipants()
    {
        return $this->participants;
    }
}