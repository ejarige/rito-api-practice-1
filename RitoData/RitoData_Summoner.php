<?php

/**
 * Class RitoData_Summoner
 * -- contains all data about 1 summoner
 */
class RitoData_Summoner
{
    private $id;
    private $name;
    private $level;

    private $rank;
    private $division;
    private $lp;
    private $win;
    private $lose;

    private $profileIconId;
    private $revisionDate;

    public function __construct($region = false,$id = false)
    {
        if($region && $id){
            $api_key = Core::API_KEY;

            // Summoner Information
            $url  = "https://euw.api.pvp.net/api/lol/$region/v1.4/summoner/$id?api_key=$api_key";
            $json = file_get_contents($url);
            $data = json_decode($json,1);

            $this->id             = $data[$id]["id"];
            $this->name           = $data[$id]["name"];
            $this->level          = $data[$id]["summonerLevel"];
            $this->profileIconId  = $data[$id]["profileIconId"];
            $this->revisionDate   = $data[$id]["revisionDate"];

            // League Information
            $url = "https://euw.api.pvp.net/api/lol/$region/v2.5/league/by-summoner/$this->id/entry?api_key=$api_key";
            $json = file_get_contents($url);
            $data = json_decode($json,1);

            $key = $this->id;

            foreach($data[$key] as $rank_data)
                if($rank_data["queue"] == "RANKED_SOLO_5x5"){
                    $this->rank     = $rank_data["tier"];
                    $this->division = $rank_data["entries"][0]["division"];
                    $this->lp       = $rank_data["entries"][0]["leaguePoints"];
                    $this->win      = $rank_data["entries"][0]["wins"];
                    $this->lose     = $rank_data["entries"][0]["losses"];
                }
        }
    }

    public function loadFromAPI($region,$name){
        $api_key = Core::API_KEY;

        // Summoner Information
        $url  = "https://euw.api.pvp.net/api/lol/$region/v1.4/summoner/by-name/$name?api_key=$api_key";
        $json = file_get_contents($url);
        $data = json_decode($json,1);

        $key = preg_replace('/\s/','',$name);

        $this->id             = $data[$key]["id"];
        $this->name           = $data[$key]["name"];
        $this->level          = $data[$key]["summonerLevel"];
        $this->profileIconId  = $data[$key]["profileIconId"];
        $this->revisionDate   = $data[$key]["revisionDate"];

        // League Information
        $url = "https://euw.api.pvp.net/api/lol/$region/v2.5/league/by-summoner/$this->id/entry?api_key=$api_key";
        $json = file_get_contents($url);
        $data = json_decode($json,1);

        $key = $this->id;

        if(isset($data[$key])){
            foreach($data[$key] as $rank_data)
                if($rank_data["queue"] == "RANKED_SOLO_5x5"){
                    $this->rank     = $rank_data["tier"];
                    $this->division = $rank_data["entries"][0]["division"];
                    $this->lp       = $rank_data["entries"][0]["leaguePoints"];
                    $this->win      = $rank_data["entries"][0]["wins"];
                    $this->lose     = $rank_data["entries"][0]["losses"];
                }
        }

        // No League data
        else
            $this->rank     = "Unranked";

        return $this;
    }

    public function load($region,$name){
        // $this->loadFromDatabase()
        return $this->loadFromAPI($region,$name);
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    public function setRank($rank)
    {
        $this->rank = $rank;
        return $this;
    }

    public function setDivision($division)
    {
        $this->division = $division;
        return $this;
    }

    public function setLp($lp)
    {
        $this->lp = $lp;
        return $this;
    }

    public function setWin($win)
    {
        $this->win = $win;
        return $this;
    }

    public function setLose($lose)
    {
        $this->lose = $lose;
        return $this;
    }

    public function setProfileIconId($profileIconId)
    {
        $this->profileIconId = $profileIconId;
        return $this;
    }

    public function setRevisionDate($revisionDate)
    {
        $this->revisionDate = $revisionDate;
        return $this;
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getLevel(){
        return $this->level;
    }

    public function getRank()
    {
        return $this->rank;
    }

    public function getDivision()
    {
        return $this->division;
    }

    public function getLp()
    {
        return $this->lp;
    }

    public function getWins()
    {
        return $this->win;
    }

    public function getLosses()
    {
        return $this->lose;
    }

    public function getTotalGames(){
        return $this->win+$this->lose;
    }

    public function getWinRatio(){
        if($this->win != 0 && $this->lose != 0)
            return 100*$this->win/($this->win+$this->lose);
        else
            return 0;
    }

    public function getIconId(){
        return $this->profileIconId;
    }

    public function getIconUrl(){
        $icon = new StaticData_ProfileIcon();
        $icon->loadFromId($this->profileIconId);
        return $icon->getUrl();
    }

    public function getRevisionDate(){
        return $this->revisionDate;
    }
}