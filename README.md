# Personal project using Riot Games API
![riot api logo](https://onecubstokage.blob.core.windows.net/partnerlogos/riot-games-155.png) https://developer.riotgames.com/

## Expectations

1. Creating a website that makes use of the Champion Mastery API.
    1. Allowing real-time search for current games
    2. Detailing all statistics relative to champion masteries for a specific summoner
    3. Generating daily-updated global ranking by champion mastery
2. Experimenting with Riot Games API
    * This is my first project using this API. I am stil learning how make use of it properly
3. Experimenting proprietary framework
    * I'm not aiming to build a complete framework, basically I just want to practice OOP and MVC pattern.

## Reality

Atm I managed to retrieve current games' and summoners' basic data in plain text using Riot Games API.
I am working on the back-end in order to optimize request numbers and MVC classes.

## Structure

The project is structured as follows:
* Config    : contains project constants for easy configuration
* Helper    : contains all View elements to be generated (mostly HTML elements I guess)
* Plugins   : external libraries (JQuery, etc)
* Query     : expected to contain all queries on DB and/or API and/or HTML forms (AJAX, JS)(still working on this)
* RitoData  : contains Riot API Models, I refer to these as "dynamic" data in contrast of static data below
* StaticData: contains Static Data from Data Dragon (e.g. champion constants, image files, etc.)
* Scripts   : JS scripts
* Styles    : CSS stylesheets
* Templates : contains HTML generation scripts
* index.php : I use this to test my templates
* Loader.php: autoload all PHP classes once

@ejarrige 2016